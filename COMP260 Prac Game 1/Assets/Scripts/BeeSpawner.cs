﻿using UnityEngine;
using System.Collections;

public class BeeSpawner : MonoBehaviour {

	public BeeMove beePrefab;
	public int nBees = 50;
	public float xMin, yMin;
	public float width, height;


	// Use this for initialization
	void Start () {
		//create the bees
		for (int i = 0; i < nBees; i++) {
		//instantiate the bees
		BeeMove bee = Instantiate (beePrefab);
			//attach to this object in this hierarchy
			bee.transform.parent = transform;
			//give the bee a name and a number
			bee.gameObject.name = "Bee " + i;

		// move the bee to a random position
		// within the bounding rectangle
			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			bee.transform.position = new Vector2 (x, y);
		}
	}

	public void DestroyBees (Vector2 centre, float radius) {
		//destroy all bees within 'radius' of 'centre'
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild (i);
			//fixed bug by adding a type conversion
			Vector2 v = (Vector2)child.position - centre;
			if (v.magnitude <= radius) {
				Destroy (child.gameObject);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
